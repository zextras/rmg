package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

// App is a struct that contains all the necessary dependencies to run the application itself
type App struct {
	cmd       *cobra.Command
	generator *Generator
	ui        *UI
}

func (a *App) run() {
	a.cmd.Run = func(cmd *cobra.Command, args []string) {
		err := a.generator.run(args[0])
		if err != nil {
			a.ui.err(fmt.Sprintln(err))
			os.Exit(1)
		}
	}
	err := a.cmd.Execute()
	if err != nil {
		a.ui.err(fmt.Sprintln(err))
		os.Exit(1)
	}
}

// NewRootCmd created a new cobra.Command that is the root of the program itself
func NewRootCmd() *cobra.Command {
	return &cobra.Command{
		Use:        "rmg [flags] DEFINITION",
		Short:      "rmg is a easy-to-go mass readme generator",
		Long:       `An easy-to-go mass readme generator to publish and create multiple READMEs for projects that don't have any`,
		Args:       cobra.ExactValidArgs(1),
		ArgAliases: []string{"YAML definition"},
		// Run param is left empty on purpose
	}
}

// NewApp creates a brand-new application
func NewApp(cmd *cobra.Command, generator *Generator, ui *UI) *App {
	return &App{
		cmd:       cmd,
		generator: generator,
		ui:        ui,
	}
}
