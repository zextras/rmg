//go:build wireinject
// +build wireinject

package main

import (
	"github.com/google/wire"
	"github.com/spf13/afero"
)

// InitUI is the injector to create UI struct
func InitUI() *UI {
	wire.Build(NewStdErr, NewStdOut, NewUI)
	return &UI{}
}

// InitGenerator is the injector to create Generator struct
func InitGenerator(fs afero.Fs) *Generator {
	wire.Build(InitUI, NewGenerator)
	return &Generator{}
}

// InitApp is the injector to create App struct
func InitApp(fs afero.Fs) *App {
	wire.Build(InitGenerator, InitUI, NewRootCmd, NewApp)
	return &App{}
}
