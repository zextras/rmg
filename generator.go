package main

import (
	"bytes"
	"fmt"
	"github.com/go-yaml/yaml"
	"github.com/pkg/errors"
	"github.com/spf13/afero"
	"io/fs"
	"io/ioutil"
	"path"
	"text/template"
)

type generatorInstructions struct {
	Version    string    `yaml:"version"`
	Template   string    `yaml:"template"`
	ReadmeName string    `yaml:"readmeName"`
	Projects   []project `yaml:"projects"`
}

type project struct {
	Name               string           `yaml:"name"`
	TemplateOverride   string           `yaml:"templateOverride,omitempty"`
	ReadmeNameOverride string           `yaml:"readmeNameOverride"`
	Content            map[string]field `yaml:"content"`
}

type field struct {
	Text string `yaml:"text,omitempty"`
	Path string `yaml:"path,omitempty"`
}

// Generator is a struct that has the goal to take in input a YAML containing the text to fill in a template and to generate the README as output.
type Generator struct {
	ui *UI
	fs afero.Fs
}

func (g *Generator) loadGenerator(configPath string) (*generatorInstructions, error) {
	runInstructions, err := g.fs.Open(configPath)
	if err != nil {
		return nil, err
	}

	runInstructionsContent, err := ioutil.ReadAll(runInstructions)
	if err != nil {
		return nil, err
	}

	instructions := &generatorInstructions{}
	err = yaml.Unmarshal(runInstructionsContent, instructions)
	if err != nil {
		return nil, err
	}

	return instructions, nil
}

func (g *Generator) run(configPath string) error {
	instructions, err := g.loadGenerator(configPath)
	if err != nil {
		return err
	}

	switch instructions.Version {
	case "1":
		return g.version1Parser(instructions)
	default:
		return errors.Errorf("version %s not supported. Supported versions are: \"1\"", instructions.Version)
	}
}

func (g *Generator) version1Parser(instructions *generatorInstructions) error {
	templateToRender, err := afero.ReadFile(g.fs, instructions.Template)
	readmeName := instructions.ReadmeName
	total := len(instructions.Projects)
	for i, project := range instructions.Projects {
		g.ui.out(NORMAL, fmt.Sprintf("[%d/%d] processing %s\n", i+1, total, project.Name))
		if err != nil {
			return err
		}

		if project.TemplateOverride != "" {
			templateToRender, err = afero.ReadFile(g.fs, project.TemplateOverride)
			if err != nil {
				return err
			}
		}

		if project.ReadmeNameOverride != "" {
			readmeName = project.ReadmeNameOverride
		}

		preparedTemplate, err := template.New(project.Name).Parse(string(templateToRender))
		if err != nil {
			return err
		}

		generatedContent := &bytes.Buffer{}
		loadedVariables := make(map[string]string)
		for k, v := range project.Content {
			content := ""
			if v.Text != "" && v.Path != "" {
				g.ui.out(QUIET, fmt.Sprintf("warning: field %s has both text con path filled: only path will be considered", k))
			}
			if v.Path != "" {
				contentFileBytes, err := afero.ReadFile(g.fs, v.Path)
				if err != nil {
					return err
				}
				content = string(contentFileBytes)
			} else if v.Text != "" {
				content = v.Text
			} else {
				return errors.Errorf("for field %s neither text or field has been filled. At least one field should be complete", k)
			}
			loadedVariables[k] = content
		}
		err = preparedTemplate.Execute(generatedContent, loadedVariables)
		if err != nil {
			return err
		}
		err = afero.WriteFile(g.fs, path.Join(project.Name, readmeName), generatedContent.Bytes(), fs.FileMode(0755))
		if err != nil {
			return err
		}
	}
	return nil
}

// NewGenerator creates a brand-new Generator
func NewGenerator(ui *UI, fs afero.Fs) *Generator {
	return &Generator{
		ui: ui,
		fs: fs,
	}
}
