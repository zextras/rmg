package main

import (
	"github.com/alecthomas/assert"
	"github.com/spf13/afero"
	"io/fs"
	"os"
	"testing"
)

func TestGenerator_loadGenerator(t *testing.T) {
	t.Parallel()
	t.Run("shouldLoadGeneratedFileWithText", func(t *testing.T) {
		fileSystem := afero.NewMemMapFs()
		generator := Generator{
			ui: NewUI(os.Stdout, os.Stderr),
			fs: fileSystem,
		}
		assert.NoError(t, afero.WriteFile(fileSystem, "test123.yaml", []byte(`version: "1"
template: "test123"
readmeName: "README.md"
projects:
  - name: "<project name>"
    templateOverride: "an override"
    readmeNameOverride: "A DIFFERENT README"
    content:
      field1:
        text: "content1"
      field2:
        text: "content2"
  - name: "<project name2>"
    content:
      field3:
        text: "content3"
      field4:
        text: "content4"`), fs.FileMode(0755)))

		loadGenerator, err := generator.loadGenerator("test123.yaml")
		assert.NoError(t, err)
		assert.NotNil(t, loadGenerator)
		assert.Equal(t, 2, len(loadGenerator.Projects))
		assert.Equal(t, "test123", loadGenerator.Template)
		assert.Equal(t, "<project name>", loadGenerator.Projects[0].Name)
		assert.Equal(t, "an override", loadGenerator.Projects[0].TemplateOverride)
		m := make(map[string]field)
		m["field1"] = field{Text: "content1"}
		m["field2"] = field{Text: "content2"}
		assert.Equal(t, m, loadGenerator.Projects[0].Content)
	})

	t.Run("shouldLoadGeneratedFileWithFileContent", func(t *testing.T) {
		fileSystem := afero.NewMemMapFs()
		generator := Generator{
			ui: NewUI(os.Stdout, os.Stderr),
			fs: fileSystem,
		}
		assert.NoError(t, afero.WriteFile(fileSystem, "path/to/file", []byte(`Hello world`), fs.FileMode(0755)))

		assert.NoError(t, afero.WriteFile(fileSystem, "test123.yaml", []byte(`version: "1"
template: "test123"
readmeName: "README.md"
projects:
  - name: "<project name>"
    templateOverride: "an override"
    readmeNameOverride: "A DIFFERENT README"
    content:
      field1:
        path: "path/to/file"
      field2:
        text: "content2"
  - name: "<project name2>"
    content:
      field3:
        text: "content3"
      field4:
        text: "content4"`), fs.FileMode(0755)))

		loadGenerator, err := generator.loadGenerator("test123.yaml")
		assert.NoError(t, err)
		assert.NotNil(t, loadGenerator)
		assert.Equal(t, 2, len(loadGenerator.Projects))
		assert.Equal(t, "test123", loadGenerator.Template)
		assert.Equal(t, "<project name>", loadGenerator.Projects[0].Name)
		assert.Equal(t, "an override", loadGenerator.Projects[0].TemplateOverride)
		m := make(map[string]field)
		m["field1"] = field{Path: "path/to/file"}
		m["field2"] = field{Text: "content2"}

		assert.Equal(t, m, loadGenerator.Projects[0].Content)
	})
}

func TestGenerator_run(t *testing.T) {
	t.Parallel()
	t.Run("shouldGenerateSingleEntryNoOverride", func(t *testing.T) {
		fileSystem := afero.NewMemMapFs()
		generator := Generator{
			ui: NewUI(os.Stdout, os.Stderr),
			fs: fileSystem,
		}
		assert.NoError(t, afero.WriteFile(fileSystem, "template.md", []byte(`# HELLO WORLD
{{ .field1 }}

This is another text`), fs.FileMode(0755)))
		assert.NoError(t, afero.WriteFile(fileSystem, "rmg.yaml", []byte(`version: "1"
template: "template.md"
readmeName: "README.md"
projects:
  - name: "testerino"
    content:
      field1:
        text: "content1"`), fs.FileMode(0755)))

		assert.NoError(t, generator.run("rmg.yaml"))

		exists, err := afero.Exists(fileSystem, "testerino/README.md")
		assert.NoError(t, err)
		assert.True(t, exists)
		got, err := afero.ReadFile(fileSystem, "testerino/README.md")
		assert.NoError(t, err)
		assert.Equal(t, `# HELLO WORLD
content1

This is another text`, string(got))
	})

	t.Run("shouldGetErrorIfVersionIsMissing", func(t *testing.T) {
		fileSystem := afero.NewMemMapFs()
		generator := Generator{
			ui: NewUI(os.Stdout, os.Stderr),
			fs: fileSystem,
		}
		assert.NoError(t, afero.WriteFile(fileSystem, "template.md", []byte(`# HELLO WORLD
{{ .field1 }}

This is another text`), fs.FileMode(0755)))
		assert.NoError(t, afero.WriteFile(fileSystem, "rmg.yaml", []byte(`template: "template.md"
readmeName: "README.md"
projects:
  - name: "testerino"
    content:
      field1:
        text: "content1"`), fs.FileMode(0755)))

		assert.Error(t, generator.run("rmg.yaml"))

		exists, err := afero.Exists(fileSystem, "testerino/README.md")
		assert.NoError(t, err)
		assert.False(t, exists)
	})

	t.Run("shouldLoadContentAsFile", func(t *testing.T) {
		fileSystem := afero.NewMemMapFs()
		generator := Generator{
			ui: NewUI(os.Stdout, os.Stderr),
			fs: fileSystem,
		}
		assert.NoError(t, afero.WriteFile(fileSystem, "template.md", []byte(`# HELLO WORLD
{{ .field1 }}

This is another text`), fs.FileMode(0755)))
		assert.NoError(t, afero.WriteFile(fileSystem, "path/to/content", []byte(`this is the content!

Yo!`), fs.FileMode(0755)))
		assert.NoError(t, afero.WriteFile(fileSystem, "rmg.yaml", []byte(`version: "1"
template: "template.md"
readmeName: "README.md"
projects:
  - name: "testerino"
    content:
      field1:
        path: "path/to/content"`), fs.FileMode(0755)))

		assert.NoError(t, generator.run("rmg.yaml"))

		exists, err := afero.Exists(fileSystem, "testerino/README.md")
		assert.NoError(t, err)
		assert.True(t, exists)
		got, err := afero.ReadFile(fileSystem, "testerino/README.md")
		assert.NoError(t, err)
		assert.Equal(t, `# HELLO WORLD
this is the content!

Yo!

This is another text`, string(got))
	})
}
