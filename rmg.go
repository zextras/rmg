package main

import "github.com/spf13/afero"

func main() {
	app := InitApp(afero.NewOsFs())
	app.run()
}
